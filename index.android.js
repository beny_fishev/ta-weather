import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    Image,
    Alert,
    TouchableHighlight,
    BackHandler,
} from 'react-native';

var styles = require('./src/styles');
var apiRequest = require('./src/api');

const constPicUrl = 'http://www.weatherunlocked.com/Images/icons/1/';

// Root class
export default class Habr01 extends Component {

    constructor(props) {
        super(props);
        this.onRefresh = this.getTelAviv.bind(this);                    // bind button
        this.state = {weather: [], isLoading: true, picUrl: constPicUrl};
    }

    componentWillMount() {          // Start request 4 data
        this.getTelAviv();
    }

    componentDidMount() {
        setInterval(() => {         // Request every 1 minute
            this.getTelAviv();
        }, 60000);
    }

    getTelAviv() {                  // Request and parse weather data

        this.setState({isLoading: true});   // for rendring ActivityIndicator

        apiRequest()
            .then((responseJson) => {
                this.setState({weather: responseJson, picUrl: constPicUrl + responseJson.wx_icon, isLoading: false,})
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    'Connection Error' + error,
                    [
                        {text: 'Exit', onPress: () => BackHandler.exitApp()},
                        {text: 'Retry', onPress: () => this.getTelAviv()}       // Retry request
                    ]
                )
                console.error('catched ' + error);
            });
    };

    renderLoading = () => (     // Activity indicator rendering
        <View style={styles.weatherImg}>
            <ActivityIndicator animating={true} size='large'/>
        </View>
    )

    renderWeather = () => (         // Rendering data to template
        <View style={styles.box2}>
            <View style={styles.box1}>
                <Text style={styles.textBig}>
                    {'  ' + this.state.weather.temp_c + '°C  '}
                </Text>
                <Text style={styles.text}>
                    {'Wind: ' + this.state.weather.windspd_ms + 'ms '}
                </Text>
            </View>
            <View style={styles.actualWeathContainer}>
                <Image source={{uri: this.state.picUrl}} style={styles.weatherImg}/>
                <View style={{margin: 24}}>
                    <Text style={styles.text}>
                        {this.state.weather.wx_desc + ' \n'}
                    </Text>
                </View>
            </View>
        </View>
    )

    render() {
        {
            var nowTime = (new Date()).toLocaleString('en', {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            });
        }
        return (
            <View style={styles.mainContainer}>

                <View style={styles.box1}>
                    <Text style={styles.text}>
                        Weather in Tel-Aviv
                    </Text>
                </View>
                <View style={styles.box1}>
                    <Text>
                        {nowTime}
                    </Text>
                </View>
                <View style={styles.box05}>
                    <Text style={styles.text}>
                        Actual weather: {'\n'}
                    </Text>
                </View>

                <View style={styles.box2}>
                    {
                        this.state.isLoading &&
                        this.renderLoading()
                    }
                    {
                        !this.state.isLoading &&
                        this.renderWeather()
                    }
                </View>
                <View style={styles.box1}>
                    <TouchableHighlight onPress={this.onRefresh}
                                        underlayColor='#BBDEFB'
                                        style={styles.touchHigh}>
                        <Image source={require('./src/ref1.jpeg')}
                            style={styles.buttonImg}/>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('Habr01', () => Habr01);