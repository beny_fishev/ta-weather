import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
} from 'react-native';

var apiMovies = require('./src/apiMovies.js');

export default class Habr01 extends Component {
    constructor(props) {
        super(props);
        /*
         this.state = {movies: []}
         */
        this.state = {weather: {}, loading: true}
    }

    componentDidMount() {
        this.getMoviesFromApiAsync();
    }

    getTelAviv() {
        this.setState({loading: true})
        return fetch('http://api.weatherunlocked.com/api/current/32.08,34.78?app_id=4749862c&app_key=813ee8e8f561dcb33b2d927e5b3cdb99', {
            method: 'get',
            headers: {'application': 'json'}
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({weather: responseJson, loading: false});
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getMoviesFromApiAsync() {
        return fetch('https://facebook.github.io/react-native/movies.json',
            {
                method: 'get',
                headers: {'application': 'json'}
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({movies: responseJson.movies});
            })
            .catch((error) => {
                console.error(error);
            });
    }

    renderMovies = () =>

        this.state.movies.map((movie, i) => <Text key={i}>{movie.title}</Text>);

    /*
     this.state.weather(() => {
     for (let key in weather) {
     <Text>
     {key + '  ' + weather[key]}
     </Text>
     }
     }
     )
     ///{weather.lat}

     renderButton=()=> { <button on-click="{getTelAviv}">Refresh</button>}
     renderLoading =() => {if state.loading => dosmth}

     this.state.weather(() => {
     <Text>
     {weather}
     </Text>
     }
     )
     */
    render() {
        return (
            <View style={styles.mainContainer}>
                {this.renderMovies()}
            </View>
        )
    }
}

var styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FC9F',
    },
})

AppRegistry.registerComponent('Habr01', () => Habr01);


// https://codedump.io/share/l8Ibnxmi66qF/1/react-nativ-networking-unexpected-token-in-function