import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
} from 'react-native';


export default class Habr01 extends Component {
    constructor(props) {
        super(props);
        this.state = { movies: [] }
    }
    componentDidMount() {
        this.getMoviesFromApiAsync();
    }
    getMoviesFromApiAsync() {
        return fetch('https://facebook.github.io/react-native/movies.json')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ movies: responseJson.movies });
            })
            .catch((error) => {
                console.error(error);
            });
    }
    renderMovies = () =>
        this.state.movies.map((movie, i) => <Text key={i}>{movie.title}</Text>)

    render() {
        return (
            <View style={styles.mainContainer}>
                {this.renderMovies()}
            </View>
        )
    };
}

var styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FC9F',
    },
})

AppRegistry.registerComponent('Habr01', () => Habr01);


// https://codedump.io/share/l8Ibnxmi66qF/1/react-nativ-networking-unexpected-token-in-function