import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    Image,
    Alert,
    TouchableHighlight,
    BackHandler,
} from 'react-native';

var styles = require('./src/styles');
var myReq = require('./src/api')

const constPicUrl = 'http://www.weatherunlocked.com/Images/icons/1/';

//Root class
export default class Habr01 extends Component {

    constructor(props) {
        super(props);
        this.onRefresh = this.getTelAviv.bind(this);
        this.state = {weather: [], isLoading: true, picUrl: constPicUrl};
    }

    componentWillMount() {
        this.getTelAviv();
    }

    componentDidMount() {
        setInterval(() => {         // Request every 1 minute
            this.getTelAviv();
        }, 60000);
    }

    getTelAviv() {                  // Request and parse weather data
        function handleErrors(response) {       // handling response error
            if (!response.ok) {
                console.error('resp !ok  ' + response.statusText);
                throw Error(response.statusText);
            }
            return response;
        }
        this.setState({isLoading: true});

        // return fetch('http://api.weatherunlocked.com/api/current/32.08,34.78?app_id=4749862c&app_key=813ee8e8f561dcb33b2d927e5b3cdb99', {
        //     method: 'get',
        //     headers: {'application': 'json'},
        // })
        //     .then(handleErrors)
        //     .then((response)=>(response.json()))

        myReq()
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({weather: responseJson})
                this.setState({picUrl: constPicUrl + responseJson.wx_icon, isLoading: false,})
            })
        // .catch((error) => {
        //     Alert.alert(
        //         'Error',
        //         'Connection Error' + error,
        //         [
        //             {text: 'Exit', onPress: () => BackHandler.exitApp()},
        //             {text: 'Retry', onPress: () => this.getTelAviv()},
        //         ]
        //     )
        //     console.error('catched ' + error);
        // });
    };

    renderLoading = () => (
        <View style={styles.weatherImg}>
            <ActivityIndicator animating={true} size='large'/>
        </View>
    )

    renderWeather = () => (         // Rendring data to teamplate
        <View style={styles.box2}>
            <View style={styles.box1}>
                <View style={styles.actualWeathContainer}>
                    <Image source={{uri: this.state.picUrl}} style={styles.weatherImg}/>
                    <Text style={[styles.text, {alignSelf: 'flex-end', marginBottom: 25}]}>
                        {this.state.weather.wx_desc + ' \n'}
                    </Text>
                </View>
            </View>
            <View style={styles.box1}>
                <Text style={styles.text}>
                    {'Temp: ' + this.state.weather.temp_c + '°C' + ' \n' +
                    'Wind: ' + this.state.weather.windspd_kmh + ' ' + this.state.weather.winddir_compass + ' \n'}
                </Text>
            </View>
        </View>
    )

    render() {
        {
            var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
            var d = new Date();
            var time = Date.toLocaleString();
            var n = d.toLocaleString('ru', options);
        }
        return (
            <View style={styles.mainContainer}>

                <View style={[styles.actualWeathContainer, styles.marginTop]}>
                    <Text style={styles.text}>
                        Tel-Aviv
                    </Text>
                </View>
                <View style={styles.box1}>
                    <Text>
                        {n}
                    </Text>
                </View>
                <View style={styles.box05}>
                    <Text style={styles.text}>
                        Actual weather: {'\n'}
                    </Text>
                </View>
                <View style={styles.box2}>
                    {
                        this.state.isLoading &&
                        this.renderLoading()
                    }
                    {
                        !this.state.isLoading &&
                        this.renderWeather()
                    }
                </View>
                <View style={styles.box1}>
                    <TouchableHighlight onPress={this.onRefresh} activeOpacity={0.7}
                                        underlayColor='#BBDEFB'
                                        style={{
                                            width: 48, height: 48, left: 0, top: 0, alignItems: 'flex-end'
                                        }}>
                        <Image
                            style={styles.button}
                            source={require('./src/ref1.jpeg')}
                        />
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('Habr01', () => Habr01);