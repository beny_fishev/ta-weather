module.exports = function () {

    var url = 'http://api.weatherunlocked.com/api/current/32.08,34.78?app_id=4749862c&app_key=813ee8e8f561dcb33b2d927e5b3cdb99';

    function handleErrors(response) {       // handling response error function
        if (!response.ok) {
            console.error('resp !ok  '+ response.status + ' ' + response._bodyText );
            throw Error(response.status + ' ' + response._bodyText);
        }
        return response;
    }

    return fetch(url, {
        method: 'get',
        headers: {'application': 'json'},
    })
        .then(handleErrors)         // handling response errors
        .then((response) => {
            return response.json();
        })
}