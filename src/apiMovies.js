module.exports = function () {

    return fetch('https://facebook.github.io/react-native/movies.json')
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ movies: responseJson.movies });
        })
        .catch((error) => {
            console.error(error);
        });
}