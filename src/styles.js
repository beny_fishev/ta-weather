var React = require('react-native');
var {StyleSheet} = React;

var styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#BBDEFB',
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0000FB',
    },
    weatherImg: {
        width: 48,
        height: 48,
        borderRadius: 8,
        borderColor: '#BDBDBD',
        alignSelf: 'center'
    },
    actualWeathContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    box05: {
        flex: 0.5,
    },
    box1: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    box2: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 32,
        fontFamily: 'Gill Sans',
    },
    textBig: {
        fontSize: 48,
        margin: 8,
        backgroundColor: '#B0BEC5',
        borderRadius: 12,
    },
    buttonImg:{
        width: 48,
        height: 48,
        borderRadius: 24,
        flexWrap: 'wrap',
    },
    toushHigh:{
        width: 48,
        height: 48,
        alignItems: 'flex-end'
    }

})

module.exports = styles;